(function () {
    angular
        .module("GroceryApp")
        .controller("GroceryCtrl", GroceryCtrl);

    GroceryCtrl.$inject = ["$http"];


    function GroceryCtrl($http) {
        var vm = this;
        vm.brand ="";

        vm.init = function(){
            $http
                .get("/api/grocery")
                .then(function (response) {
                    console.log("init data " + JSON.stringify(response.data));
                    vm.list = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
        vm.init();

        vm.search = function() {

            $http
                .get("/api/grocery",
                    {params: {'brand': vm.brand}})
                .then(function (response) {
                    vm.list = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }
    };

})();

