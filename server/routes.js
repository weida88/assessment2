'use strict';
var express = require("express");
var path = require("path");

var GroceryController = require("./api/grocery.controller");

const CLIENT_FOLDER = path.join(__dirname + '/../client');


module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
}

function configureRoutes(app){
    app.get("/api/grocery", GroceryController.list);
    app.get("/api/grocery/:brand", GroceryController.get);

    app.use(express.static(CLIENT_FOLDER));
}


function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/errors/500.html'));
    });
};
