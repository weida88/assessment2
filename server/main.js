var express = require("express");

var app = express();
var routes = require("./routes");
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json());

routes.init(app);
routes.errorHandler(app);

app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});
