var Sequelize = require("sequelize");

module.exports = function (database) {
    var Grocery = database.define("grocery_lists", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        upc12: {
            type: Sequelize.BIGINT
        },
        brand: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });
    return Grocery;
};
