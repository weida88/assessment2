var Grocery = require("../database").Grocery;

exports.list = function(req,res){
    console.log("reached database >> list 1st 20 contacts");
    console.log(req.body);
    Grocery
        .findAll({
            limit: 20,
            order: "name ASC"
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.get = function(req, res){
    console.log("search by brand >> list 1st 20 contacts");
    var where = {};

    if (req.params.brand) {
        where.brand = req.params.brand
    }

    console.log("where: " + JSON.stringify(where));
    Grocery
        .findAll({
            where: where,
            limit: 20,
            order: "name ASC"
        })
        .then(function (result) {
            res.json(result);
        })
        .catch(function (error) {
            console.log(error);
            res
                .status(500)
                .json({error: true})
        });
};
